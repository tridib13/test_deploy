import AppLoading from 'expo-app-loading';
import {Asset} from 'expo-asset';
import React, {useState} from 'react';
import * as Font from 'expo-font';

import Icons from './utils/Icons';
import {Image} from 'react-native';
import RootNavigator from './components/navigations/RootStackNavigator';
import RootProvider from './providers';

function cacheImages(images: Image[]): Array<Promise<boolean | Asset>> {
  return images.map((image) => {
    if (typeof image === 'string') return Image.prefetch(image);
    else return Asset.fromModule(image).downloadAsync();
  });
}

const loadFonts = async (): Promise<void> => {
  return Font.loadAsync({
    avenir: require('../assets/fonts/avenir.otf'),
    avenirBold: require('../assets/fonts/avenir-bold.otf'),
    futura: require('../assets/fonts/futura.ttf'),
  });
};

const loadAssetsAsync = async (): Promise<void> => {
  const imageAssets = cacheImages(Icons);

  const fontAssets = loadFonts();

  await Promise.all([...imageAssets, fontAssets]);
};

function App(): React.ReactElement {
  return <RootNavigator />;
}

function ProviderWrapper(): React.ReactElement {
  const [loading, setLoading] = useState(true);

  if (loading) {
    return (
      <AppLoading
        startAsync={loadAssetsAsync}
        onFinish={(): void => setLoading(false)}
        onError={console.warn}
      />
    );
  }
  return (
    <RootProvider>
      <App />
    </RootProvider>
  );
}

export default ProviderWrapper;
