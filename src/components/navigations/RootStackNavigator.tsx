import {
  StackNavigationProp,
  createStackNavigator,
} from '@react-navigation/stack';
import {ThemeType, useTheme} from '../../providers/ThemeProvider';

import Intro from '../pages/Intro';
import {NavigationContainer} from '@react-navigation/native';
import React, {useEffect} from 'react';
import Temp from '../pages/Temp';

import {request} from '../../apis';

// TODO: Remove below line
import ComponentLibrary from '../pages/ComponentLibrary';

export type RootStackParamList = {
  default: undefined;
  Intro: undefined;
  Temp: {param: string};
  ComponentLibrary: undefined;
};

export type RootStackNavigationProps<
  T extends keyof RootStackParamList = 'default'
> = StackNavigationProp<RootStackParamList, T>;

const Stack = createStackNavigator<RootStackParamList>();

function RootNavigator(): React.ReactElement {
  const {theme, themeType} = useTheme();

  // TODO Remove this once app is established
  // Example of how to use the API
  useEffect(() => {
    type PingResponse = {message: string};
    request<PingResponse>('/').then(console.log);
  });

  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Intro"
        screenOptions={{
          headerStyle: {
            backgroundColor: theme.header,
          },
          headerTitleStyle: {color: theme.headerText},
          headerTintColor: theme.headerText,
        }}
        headerMode={themeType === ThemeType.DARK ? 'screen' : 'float'}>
        <Stack.Screen name="Intro" component={Intro} />
        <Stack.Screen name="Temp" component={Temp} />
        {/* //TODO: Remove ComponentLibrary line */}
        <Stack.Screen name="ComponentLibrary" component={ComponentLibrary} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default RootNavigator;
