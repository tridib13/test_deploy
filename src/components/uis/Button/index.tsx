import type {
  ImageSourcePropType,
  ImageStyle,
  StyleProp,
  TextStyle,
  ViewStyle,
} from 'react-native';

import ButtonDisabled from './ButtonDisabled';
import ButtonLoading from './ButtonLoading';
import ButtonWrapper from './ButtonWrapper';
import ButtonWithBg from './ButtonWithBg';
import type {FC, ReactElement} from 'react';
import React from 'react';

interface Props {
  testID?: string;
  isLoading?: boolean;
  isDisabled?: boolean;
  withBackground?: boolean;
  onPress?: () => void;
  style?: ViewStyle;
  disabledStyle?: ViewStyle;
  textStyle?: TextStyle;
  imgLeftSrc?: ImageSourcePropType;
  imgLeftStyle?: StyleProp<ImageStyle>;
  indicatorColor?: string;
  activeOpacity?: number;
  text?: string;
  icon?: ReactElement;
  bgImage?: ImageSourcePropType;
}

const Button: FC<Props> = ({
  testID,
  isLoading,
  isDisabled,
  withBackground,
  onPress,
  style,
  disabledStyle,
  textStyle,
  imgLeftSrc,
  imgLeftStyle,
  indicatorColor,
  activeOpacity,
  text,
  icon,
  bgImage,
}) => {
  if (isDisabled)
    return <ButtonDisabled style={disabledStyle} textStyle={textStyle} />;

  if (isLoading) return <ButtonLoading indicatorColor={indicatorColor} />;

  //These are all required for button w Bg hence check
  if (withBackground && bgImage && onPress && text)
    return (
      <ButtonWithBg icon={icon} image={bgImage} style={style} onPress={onPress}>
        {text}
      </ButtonWithBg>
    );

  return (
    <ButtonWrapper
      testID={testID}
      onPress={onPress}
      imgLeftSrc={imgLeftSrc}
      style={style}
      imgLeftStyle={imgLeftStyle}
      activeOpacity={activeOpacity}
      text={text}
      textStyle={textStyle}
      icon={icon}
    />
  );
};

export default Button;
